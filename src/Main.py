import time
import RPi.GPIO as GPIO
from RancilioManager import RancilioManager
from ServerManager import ServerManager
from Configuration import Config


def run():
    # Load any settings
    Config.load_settings()

    # Create the process controllers
    device = RancilioManager()
    server = ServerManager(device_manager=device)

    # Start the background processes
    processes = []
    processes += device.start_async()
    processes += server.start_async()

    # Notify to terminal
    if all([p.isAlive() for p in processes]):
        print("Rancilio background process managers successfully initialized")

    # Monitor processes
    # TODO: More useful process handling
    while all([p.isAlive() for p in processes]):
        time.sleep(1)


if __name__ == '__main__':
    try:
        run()
    except KeyboardInterrupt:
        pass
    finally:
        GPIO.cleanup()
        print(f"Program Complete")

