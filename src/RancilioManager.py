from PID import PID
import RPi.GPIO as GPIO
import time
from AutoTune import PIDAutotune
from enum import Enum
from Configuration import Config
from TemperatureSensor import TemperatureSensor
from PressureSensor import PressureSensor
from Relay import Relay
from threading import Thread
import math

class RancilioManager(object):
    def __init__(self):
        # Init silvia components
        self._water_temperature = TemperatureSensor()
        self._water_pressure = PressureSensor()
        self._water_boiler = Relay(control_pin=Config.pin_relay_boiler)
        self._water_pump = Relay(control_pin=Config.pin_relay_pump)
        
        # Initial machine state
        self._state = State.Unknown
        self._faults = []
        self._pwm = 0

        # Brew process
        self._brew_process = Thread(target=self.__brew_process_cycle, daemon=True, name="Device Brew Process")
        self._brewing_interrupt = True

        # Create the pid for temp control
        self.__configure_pid()
        self._auto_tune = False

        # Create callback methods
        self._heating_callback = None
        self._sensor_data_callback = None
        self._auto_tune_callback = None

    # Starts both sensor and boiler loops asynchronously
    def start_async(self):
        # Create threads for maintaining the silvia
        sensor_process = Thread(target=self.__sensor_process_loop, args=(Config.sensor_update_interval,), daemon=True, name="Device Sensor Process")
        boiler_process = Thread(target=self.__boiler_process_loop, args=(Config.boiler_update_interval,), daemon=True, name="Device Boiler Process")
        
        # Start the system control loops
        sensor_process.start()
        boiler_process.start()

        # Return the threads
        return sensor_process, boiler_process

    # The main loop for updating sensor data and fault detection
    def __sensor_process_loop(self, interval):
        while True:
            # Update the timestamp
            t = time.monotonic()

            # Update sensor data
            _ = self._water_pressure.update_accumulator()
            _ = self._water_temperature.update_accumulator()

            # Uncomment to log data
            # with open('/home/pi/rancilio/log_file.csv', 'a') as file:
            #     w = csv.writer(file)
            #     w.writerow([t, temp, self._water_temperature.get_latest_reading(filtered=True)])

            # Check for faults
            self._faults += [fault for fault in self._water_pressure.get_faults() if fault not in self._faults]
            self._faults += [fault for fault in self._water_temperature.get_faults() if fault not in self._faults]
            self._state = State.Fault if self._faults else State.Nominal

            # Callback method for latest sensor data
            if callable(self._sensor_data_callback):
                self._sensor_data_callback(self.get_sensor_data())

            # Wait for update interval to pass
            time.sleep(max(0, interval - (time.monotonic() - t)))

    # The main loop for boiler temp regulation
    def __boiler_process_loop(self, interval):
        while True:
            # Latest temperature
            temp = self._water_temperature.get_latest_reading(filtered=True)

            # Update the pwm set-point
            if self._pid.auto_mode:
                self._pwm = self._pid.update(temp)
            elif self._auto_tune:
                self._pwm = self.__step_pid_autotune(temp)

            # Toggle boiler on/off for partial or complete interval
            if 0 < self._pwm < 100:
                duty_cycle = self._pwm * 0.01
                self.__toggle_boiler(True)
                time.sleep(interval * duty_cycle)
                self.__toggle_boiler(False)
                time.sleep(interval * (1 - duty_cycle))
            else:
                self.__toggle_boiler(self._pwm != 0)
                time.sleep(interval)

    # The main brew process control loop.
    def __brew_process_cycle(self, callback=None):
        print("Starting brew cycle:")

        # Start time and whole second
        t0, s = time.monotonic(), 0
        infusing, soaking, brewing = False, False, False

        # Event timing
        t_infusion = Config.time_infuse
        t_soak = t_infusion + Config.time_soak
        t_brew = t_soak + Config.time_brew

        while True:
            # Check interlock
            if self._brewing_interrupt:
                self._water_pump.disable()
                return

            # Delta time
            dt = time.monotonic() - t0

            if not infusing:
                self._water_pump.enable()
                print(' * Infusion', end='', flush=True)
                infusing = True
            elif dt > t_infusion and not soaking:
                self._water_pump.disable()
                print('\n * Soaking', end='', flush=True)
                soaking = True
            elif dt > t_soak and not brewing:
                self._water_pump.enable()
                print('\n * Brewing', end='', flush=True)
                brewing = True
            elif dt > t_brew:
                self._water_pump.disable()
                break
            else:
                sec = math.floor(dt)
                if sec > s:
                    print(".", end='', flush=True)
                    s = sec

            # Wait 100ms
            time.sleep(0.1)

        # Reset console register
        print()

        # Fire any callback
        if callable(callback):
            callback()

    # Toggles the boiler on or off
    def __toggle_boiler(self, enable):
        if enable:
            self._water_boiler.enable()
        else:
            self._water_boiler.disable()

        # Fire callback method.
        # TODO: Ensure callback is an async method
        if callable(self._heating_callback):
            self._heating_callback({'time': time.time() * 1000, 'heating': enable})

    # Instantiate the pid objects
    def __configure_pid(self):

        # Create the pid object
        self._pid = PID(
            Kp=Config.kp,
            Ki=Config.ki,
            Kd=Config.kd,
            setpoint=Config.temp_brew,
            output_limits=(0, 100),
            proportional_on_measurement=False)

        # Creates the pid auto tune object
        self.pid_tuner = PIDAutotune(
            setpoint=Config.temp_brew,
            lookback=300,
            out_step=100,
            sampletime=1,
            out_min=0,
            out_max=100,
            time=time.monotonic)

    # Update the auto tune process
    def __step_pid_autotune(self, measurement):
        # Step the autotuner
        self._auto_tune = not self.pid_tuner.run(measurement)
        print(".", end='', flush=True)

        # Check if tuning complete
        if self.pid_tuner.state == self.pid_tuner.STATE_SUCCEEDED:
            print("\nAuto tuning complete...")

            # Default to some overshoot values
            values: PIDAutotune.PIDParams = self.pid_tuner.get_pid_parameters("some-overshoot")

            # Set values to pid
            self.set_pid_gain(*values)

            # Fire any callback
            if callable(self._auto_tune_callback):
                self._auto_tune_callback(list(values))
        elif self.pid_tuner.state == self.pid_tuner.STATE_FAILED:
            print("Auto tuning failed")

        # Return the output
        return self.pid_tuner.output

    # Formats high frequency data into dict
    def get_sensor_data(self):
        data = {'time': time.time() * 1000,
                'temperature': self._water_temperature.get_latest_reading(filtered=True),
                'pressure': self._water_pressure.get_latest_reading(filtered=True),
                'pwm_value': self._pwm,
                'pid_output': self._pid.terms
                }
        return data
    
    # Formats low frequency data into dict
    def read_device_state(self):
        data = {'target': self._pid.setpoint,
                'state': self._state.name,
                'brewing': self._brew_process.is_alive(),
                'pid_values': self._pid.gains,
                'pid_auto_mode': self._pid.auto_mode,
                'pwm': self._pwm}
        return data

    # Starts the pid autotune process
    def run_auto_tune(self):
        self._auto_tune = True
        print("Starting PID Auto Tuning")

    # Starts the brew cycle
    def start_brewing_async(self, callback=None):
        # Check if brew_process thread is already running
        if self._brew_process.is_alive():
            print("Currently brewing")
            return

        # Create brew process task and start
        self._brewing_interrupt = False
        self._brew_process = Thread(target=self.__brew_process_cycle, args=(callback,), daemon=True, name="Device Brew Process")
        self._brew_process.start()

    # Terminates the brew cycle
    def stop_brewing_async(self):
        # Only stop if task is alive
        if self._brew_process.is_alive():
            self._brewing_interrupt = True
            self._brew_process.join()
            print("\nBrewing was stopped early")

    # Register callback methods
    def register_callback(self, callback, fun):
        known_callbacks = ["sensor", 'boiler', 'auto_tune']
        if callback not in known_callbacks:
            raise AssertionError(f"Callback {callback} must be one of the following: {known_callbacks}")
        elif not callable(fun):
            raise AssertionError(f"Callback {callback} must be callable")

        if callback is known_callbacks[0]:
            self._sensor_data_callback = fun
        elif callback is known_callbacks[1]:
            self._heating_callback = fun
        elif callback is [known_callbacks[2]]:
            self._auto_tune_callback = fun

    # Get any faults
    def get_faults(self):
        return self._faults

    # Clear any faults
    def clear_faults(self):
        self._faults.clear()

    # Get brewing state
    def is_brewing(self):
        return self._brew_process.is_alive()

    # Set pid setpoint
    def set_target_temp(self, value):

        # Ensure setpoint is a number above zero
        if isinstance(value, (int, float)) and value > 0:
            self._pid.setpoint = value

        # Return actual setpoint
        return self._pid.setpoint

    # Set pid gain
    def set_pid_gain(self, kp=None, ki=None, kd=None):

        # Ensure pid gains are numbers
        if isinstance(kp, (int, float)):
            self._pid.Kp = kp
        if isinstance(ki, (int, float)):
            self._pid.Ki = ki
        if isinstance(kd, (int, float)):
            self._pid.Kd = kd

        # Return actual pid values
        return self._pid.gains

    # Set pwm value
    def set_boiler_pwm(self, value):

        # Only set the boiler pwm when not in auto mode
        if not self._pid.auto_mode:
            # Clamp pwm between 0 and 100
            self._pwm = min(100, max(0, value))
            print(f"Boiler PWM set to {self._pwm}")

        # Return the actual pwm
        return self._pwm

    # Toggle for pid auto mode
    def toggle_pid_auto(self):
        enabled = not self._pid.auto_mode
        i_term = self._pwm if self._pid.Ki is not 0 else 0
        self._pid.set_auto_mode(enabled, i_term)
        print(f"PID auto mode {'enabled' if enabled else 'disabled'}")

        # Return the actual auto mode
        return self._pid.auto_mode

# Enum class for state of the machine
class State(Enum):
    Nominal = 0,
    Fault = 1,
    Unknown = 2


# Testing
if __name__ == "__main__":
    def poll_device(poll_method):
        for i in range(50):
            reading = poll_method()
            print("Measurement {0}".format(i))
            print(f"\tTarget Temp: {reading['target']}")
            print(f"\tCurrent Temp: {reading['temperature']}")
            print(f"\tIs Heating: {reading['heating']}")
            print(f"\tPressure: {reading['pressure']}")
            print(f"\tState: {reading['state']}")
            if reading['state'] == 'Fault':
                print(f"\t{manager.get_faults()}")
            time.sleep(0.1)

    manager = RancilioManager()
    manager.start_async()
    # p = Thread(target=poll_device, args=(manager.read_device,), daemon=True)
    # p.start()
    # p.join()
    # print("\nComplete")
