
import time
import math

def sleep_with_interlock(message, time_to_sleep, interlock):
    print(message, end='', flush=True)
    t, dt, s = time.monotonic(), 0, 0
    while dt <= time_to_sleep:
        if interlock():
            return True
        dt = time.monotonic() - t
        sec = math.floor(dt)
        if sec > s:
            print(".", end='', flush=True)
            s = sec
        time.sleep(0.01)
    print()
    return False

def round_nearest(x, a):
    return round(x / a) * a