import time
from Filtering import FilterBuLp2 as DFilter
from Filtering import butterworth_lp_2nd_order as coefficients
from HelperFunctions import round_nearest

class PID:
    def __init__(self, Kp, Ki, Kd, setpoint=0, proportional_on_measurement=False, output_limits=(-float("inf"), float("inf"))):
        # Gains
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd

        # Setpoint and change in setpoint
        self._r = setpoint

        # Working attributes for sample_t-n
        self._y1 = None
        self._e1 = 0
        self._u1 = 0
        self._t1 = 0
        self._p1 = 0
        self._i1 = 0
        self._d1 = 0

        self._u_min = output_limits[0]
        self._u_max = output_limits[1]

        # Smoothing attributes
        self._d_filter = DFilter(coefficients[0.05])  # Filter for smoothing d term
        self._a = 0.80                                # Alpha used for smoothing d term

        # Options
        self.__dynamic_gain = False
        self._auto = True
        self._pom = proportional_on_measurement
        self._antiwindup = True

    @property
    def auto_mode(self):
        return self._auto

    def set_auto_mode(self, enabled, output=None):

        # If was disabled and now enabled
        if not self._auto and enabled:

            # Reset pid and set i term to prevent windup
            self.reset()
            self._i1 = 0 if output is None else output

        self._auto = enabled

    @property
    def setpoint(self):
        return self._r

    @setpoint.setter
    def setpoint(self, value):
        self._r = value

    @property
    def terms(self):
        return [self._p1, self._i1, self._d1]

    @property
    def gains(self):
        return [self.Kp, self.Ki, self.Kd]

    def update(self, y0):
        # Initialize pid data and return
        if self._y1 is None:
            if y0 is not 0:
                self._t1 = time.monotonic()
                self._y1 = round_nearest(y0, 0.1)
            return 0

        # Calculate delta time
        t0 = time.monotonic()
        dt = t0 - self._t1

        # Measurement error
        e0 = self._r - y0

        # Dynamic gains
        if self.__dynamic_gain:
            kp = self.Kp * 1
            ki = self.Ki * 1
            kd = self.Kd * 1
        else:
            kp = self.Kp
            ki = self.Ki
            kd = self.Kd

        # Proportional on error / measurement response
        # Clamp PoM output to prevent windup
        if self._pom:
            p0 = PID._clamp(self._p1 - kp * (y0 - self._y1), self._u_min, self._u_max)
        else:
            p0 = kp * e0

        # Integral response
        # Integral error scaled by current Ki inside integral to prevent on the fly tuning transients
        # Conditional integration and clamp to prevent windup
        if self._antiwindup:
            i0 = 0
        else:
            i0 = PID._clamp(self._i1 + ki * e0 * dt, -5, 10)

        # Derivative on y0
        # d_term = -kd * delta_measurement / delta_time

        # Derivative on measurement with kick management and Butterworth low pass filter
        d0 = -kd * (y0 - self._y1) / dt
        d0 = self._d_filter.step(d0)

        # Derivative on error with alpha smoothing
        # d_term = kd * (self._a * (e0 - self._e1) / delta_time + (1 - self._a) * self._e1)

        # Clamp response between ranges to prevent windup
        u0 = PID._clamp(p0 + i0 + d0, self._u_min, self._u_max)

        # Conditional integral antiwindup measures
        self._antiwindup = (u0 == self._u_max and e0 * u0 > 0) or abs(e0) > 10

        # Update working values
        self._t1 = t0
        self._y1 = y0
        self._e1 = e0
        self._p1 = p0
        self._d1 = d0
        self._i1 = i0
        self._u1 = u0

        # Return response
        return u0

    @ staticmethod
    def _clamp(value, lower, upper):
        if value > upper:
            return upper
        elif value < lower:
            return lower
        else:
            return value

    def reset(self):
        self._p1 = 0
        self._i1 = 0
        self._d1 = 0
        self._y1 = None
        self._u1 = 0
        self._e1 = 0
        self._t1 = time.monotonic()


class PID2:
    def __init__(self, Kp, Ki, Kd, setpoint=0, proportional_on_measurement=False, output_limits=(-float("inf"), float("inf")), N=1):

        # Gains
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd

        # Working terms
        self._e0, self._e1 = 0, 0    # Error, Error-1
        self._u0, self._u1 = 0, 0    # Output, Output-1
        self._y0, self._y1 = 0, 0    # Measurement, Measurement-1
        self._t1 = time.monotonic()  # Time-1

        # Setpoint
        self._r = setpoint

        # Filter power
        self._N = N

        # Output limits
        self._uMin = output_limits[0]
        self._uMax = output_limits[1]

        # Auto mode
        self._auto_mode = True

        # Latest term values
        self._p_term = 0
        self._i_term = 0
        self._d_term = 0

    @staticmethod
    def _clamp(value, lower, upper):
        if value > upper:
            return upper
        elif value < lower:
            return lower
        else:
            return value

    @property
    def auto_mode(self):
        return self._auto_mode

    def set_auto_mode(self, enabled, output=None):

        # If was disabled and now enabled
        if not self._auto_mode and enabled:
            # Reset pid and set i term to prevent windup
            self.reset()
            self._i_term = 0 if output is None else output

        self._auto_mode = enabled

    @property
    def setpoint(self):
        return self._r

    @setpoint.setter
    def setpoint(self, value):
        self._r = value

    @property
    def terms(self):
        return [self._p_term, self._i_term, self._d_term]

    @property
    def gains(self):
        return [self.Kp, self.Ki, self.Kd]

    def reset(self):
        self._p_term = 0
        self._i_term = 0
        self._d_term = 0
        self._y1 = 0
        self._u1 = 0
        self._e1 = 0
        self._t1 = time.monotonic()

    def update(self, y):

        # Shift time
        t0 = time.monotonic()
        Ts = t0 - self._t1
        self._t1 = t0

        # Cache instance variables within scope
        N, Kp, Ki, Kd = self._N, self.Kp, self.Ki, self.Kd

        # Update coefficients. This can be precomputed for speed, but I have it here now to incorporate real dt
        a0 = (1 + N * Ts)
        a1 = -(2 + N * Ts)
        a2 = 1
        b0 = Kp * (1 + N * Ts) + Ki * Ts * (1 + N * Ts) + Kd * N
        b1 = -(Kp * (2 + N * Ts) + Ki * Ts + 2 * Kd * N)
        b2 = Kp + Kd * N
        ku1 = a1 / a0
        ku2 = a2 / a0
        ke0 = b0 / a0
        ke1 = b1 / a0
        ke2 = b2 / a0

        # Shift error history
        e2 = self._e1
        self._e1 = self._e0
        self._e0 = self._r - y

        # Shift output history
        u2 = self._u1
        self._u1 = self._u0

        # Plant output
        u0 = -ku1 * self._u1 - ku2 * u2 + ke0 * self._e0 + ke1 * self._e1 + ke2 * e2

        # Clamp output ot limits
        self._u0 = self._clamp(u0, self._uMin, self._uMax)

        # Return output
        return self._u0








