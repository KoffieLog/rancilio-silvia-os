from MCP3201 import MCP3201
from Sensor import Sensor
import time
from itertools import compress

'''
Handles the polling of sensor data from a pressure transducer
RATING defines the upper pressure limit of the transducer (250PSI)
SCALE defines unit conversion of RATING to returned pressure (1PSI = 0.06894757BAR)
'''
class PressureSensor(Sensor):

    # Error codes for Honeywell P3X series pressure transducer
    _fault_table = [
        "EEPROM Corrupt / Low Supply Voltage",
        "Sensor Bridge Open / Short"]

    def __init__(self, vref=5.0, rating=250, scale=0.06894757):

        # Initialize the base sensor class
        super().__init__(
            accumulator_size=10)

        # Pressure transducer uses MCP3201 for ADC
        self._adc = MCP3201(spi_bus=0, ce_pin=0, vref=vref)

        # Characteristics to Honeywell P3X pressure transducer
        pmin = 0
        pmax = rating * scale
        vmin = 0.5
        vmax = 4.5
        self._dpdv = (pmax-pmin) / (vmax-vmin)  # change in pressure per voltage
        self._voff = vmin                       # voltage offset of min pressure

        # Fault thresholds
        self._lower_rail = 0.025 * vmax
        self._upper_rail = 0.975 * vmax
        self._faults = [False, False]

    def _read_sensor(self):
        voltage = self._adc.read_voltage(mode="LSB")
        self._check_faults(voltage)
        pressure = (voltage - self._voff) * self._dpdv
        return round(pressure, 3)

    def _check_faults(self, voltage):
        self._faults = [False, False]
        if voltage < self._lower_rail:
            self._faults[0] = True
        if voltage > self._upper_rail:
            self._faults[1] = True

    def _get_voltage(self):
        return self._adc.read_voltage(mode="LSB")

    def _get_bin(self):
        return self._adc.read_bin()

    def get_faults(self):
        return list(compress(PressureSensor._fault_table, self._faults))


# Testing
if __name__ == '__main__':
    sensor = PressureSensor()

    for i in range(11):
        # Update accumulator
        pres, _ = sensor.update_accumulator()
        faults = sensor.get_faults()

        print(f"Reading {i}")
        print(f"\tPressure: {pres}")
        print(f"\tFaults: {faults}")
        time.sleep(0.1)

    pres = sensor.get_latest_reading(filtered=True)
    print(f"\nFiltered pressure: {pres}")

#     input("\nPress Enter to continue...")
    while True:
        sensor.update_accumulator()
        # print(sensor.get_latest_reading(FILTER_READING=False))
        print(sensor._get_voltage())
        time.sleep(1/10)
