from threading import Lock
from Filtering import Filter, low_pass_filter_coefficients

'''
Class to store sensor data '''
class Sensor(object):

    def __init__(self, accumulator_size):
        # TODO: Kalman filtering
        # Create a low pass filter
        self.__filter = Filter(accumulator_size, low_pass_filter_coefficients)

        # Sensor poling lock to keep threads from interfering with read / write
        self.__lock = Lock()

    def update_accumulator(self):
        # Collect new sample under lock
        with self.__lock:
            sample = self._read_sensor()

        # Update the filter
        self.__filter.add_sample(sample, filter_samples=True)

        # Return the newest sample
        return sample

    def get_latest_reading(self, filtered=False):
        # Use filtered or current reading
        if filtered:
            return self.__filter.get_filtered_value()

        with self.__lock:
            return self._read_sensor()

    def _read_sensor(self):
        return 0

    def get_faults(self):
        return []
