import json


''' 
Container to store any user definable variables.
Save and load methods dump and load all declared class attributes to JSON allowing robust persistence
'''
class Config:

    # Target water temperatures
    temp_brew = 105

    # Brew and preinfusion time
    time_infuse = 2
    time_soak = 3
    time_brew = 25

    # PID gains
    kp = 7.5
    ki = 0.06
    kd = 165

    # IO Pin assignment
    pin_relay_boiler = 13
    pin_relay_pump = 12
    pin_sensor_temp = 5

    # Control process update intervals [seconds]
    server_update_interval = 0.5
    sensor_update_interval = 0.1
    boiler_update_interval = 1.0

    @classmethod
    def save_settings(cls):
        with open('/home/pi/rancilio/saved_config.json', 'w') as file:
            data = {key: value for key, value in cls.__dict__.items() if not key.startswith(("__", "save", "load"))}
            json.dump(data, file)

    @classmethod
    def load_settings(cls):
        try:
            with open('/home/pi/rancilio/saved_config.json', 'r') as file:
                for key, value in json.load(file).items():
                    setattr(cls, key, value)
        except FileNotFoundError:
            Config.save_settings()


# Test
if __name__ == '__main__':
    Config.load_settings()
    print(Config.temp_brew)
    Config.temp_brew = 150
    Config.save_settings()
    Config.load_settings()
    print(Config.temp_brew)