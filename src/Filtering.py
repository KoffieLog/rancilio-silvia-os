import time
from math import exp
from threading import Lock

'''
FIR filter given one of the filter coefficients and a window length
'''
class Filter(object):
    def __init__(self, accumulator_size, coefficients):
        # Attributes
        self.__coefficients = coefficients
        self.__accumulator = [0] * accumulator_size
        self.__accumulator_size = accumulator_size
        self.__accumulator_lock = Lock()
        self.__nSamples = 0
        self.__filtered_value = 0

    def add_sample(self, sample, filter_samples=True):
        # Update accumulator under lock
        with self.__accumulator_lock:
            # Place newest data at the front of the accumulator and remove oldest sample
            self.__accumulator.insert(0, sample)
            self.__accumulator.pop()

            # Increment sample count
            self.__nSamples += 1

        # Filter the accumulator array right away if flagged else this needs to be done later on
        if filter_samples:
            self.filter_accumulator()

    def filter_accumulator(self):
        # Filter accumulator under lock
        with self.__accumulator_lock:

            # Filter the accumulator based on number of data points collected
            if self.__nSamples >= self.__accumulator_size:
                sig = self.__accumulator
                filt = self.__coefficients[self.__accumulator_size]
                self.__filtered_value = sum(a * c for a, c in zip(sig, filt))
            elif self.__nSamples > 2:
                sig = self.__accumulator[0:self.__nSamples]
                filt = self.__coefficients[len(sig)]
                self.__filtered_value = sum(a * c for a, c in zip(sig, filt))
            else:
                self.__filtered_value = 0

    def debug(self):
        return print("LeastSquareFilter\n\tSamples: {},\n\tAccumulator: {}\n\tFiltered value: {}".format(self.__nSamples, self.__accumulator, self.__filtered_value))

    # TODO: Implement MAD outlier rejection into filtering
    # @staticmethod
    # def __reject_outliers(data, m=2.):
    #     d = abs(data - median(data))
    #     mdev = median(d)
    #     s = d / mdev if mdev else 0.
    #     return data[s < m]

    def get_filtered_value(self):
        return self.__filtered_value


'''
A simple IIR Filter
'''
class ExponentialWindow(object):
    def __init__(self, tau):
        self._tau = tau
        self._value = None
        self._timestamp = time.time()
        self._nSamples = 0

    def add_sample(self, sample):
        # Latest timestamp
        t = time.time()
        dt = t - self._timestamp
        self._timestamp = t

        # Update value
        if self._value is not None:
            self._value = self._value + self._alpha(dt) * (sample-self._value)
        else:
            self._value = sample

        self._nSamples += 1
        return self._value

    def _alpha(self, dt):
        return 1 - exp(-dt / self._tau)

    def _alpha_approx(self, dt):
        return dt / self._tau

    @property
    def value(self):
        return self._value

    @property
    def timestamp(self):
        return self._timestamp

    def samples(self):
        return self._nSamples


'''
Various filter coefficients. Plenty of libraries can generate these as well.
'''

# Linear Savitzky�Golay filter coefficients for various window sizes.
linear_savgol_coefficients = {
    2: [1.00000, 0.00000],
    3: [0.83333, 0.33333, -0.16667],
    4: [0.70000, 0.40000, 0.10000, -0.20000],
    5: [0.60000, 0.40000, 0.20000, 0.00000, -0.20000],
    6: [0.52381, 0.38095, 0.23810, 0.09524, -0.04762, -0.19048],
    7: [0.46429, 0.35714, 0.25000, 0.14286, 0.03571, -0.07143, -0.17857],
    8: [0.41667, 0.33333, 0.25000, 0.16667, 0.08333, 0.00000, -0.08333, -0.16667],
    9: [0.37778, 0.31111, 0.24444, 0.17778, 0.11111, 0.04444, -0.02222, -0.08889, -0.15556],
    10: [0.34545, 0.29091, 0.23636, 0.18182, 0.12727, 0.07273, 0.01818, -0.03636, -0.09091, -0.14545]
}

# Low pass filter <1Hz
low_pass_filter_coefficients = {
    3: [0.0689264, 0.86214719, 0.0689264],
    4: [0.04700274, 0.45299726, 0.45299726, 0.04700274],
    5: [0.03563836, 0.24103457, 0.44665414, 0.24103457, 0.03563836],
    6: [0.02868081, 0.14301034, 0.32830886, 0.32830886, 0.14301034, 0.02868081],
    7: [0.02397987, 0.09322873, 0.23202599, 0.30153081, 0.23202599, 0.09322873, 0.02397987],
    8: [0.02058851, 0.06541966, 0.16640878, 0.24758305, 0.24758305, 0.16640878, 0.06541966, 0.02058851],
    9: [0.01802459, 0.04860453, 0.12263296, 0.19688947, 0.22769688, 0.19688947, 0.12263296, 0.04860453, 0.01802459],
    10: [0.01601677, 0.03776234, 0.09297631, 0.15600355, 0.19724103, 0.19724103, 0.15600355, 0.09297631, 0.03776234, 0.01601677]
}


'''
Some Butterworth filters and associated coefficients for various order/alpha pairs
'''

# Low pass butterworth filter order=1 alpha1=0.05
class FilterBuLp1:

    def __init__(self, coefficients):
        self.v = [0] * 2
        self._coefs = coefficients

    def step(self, x):
        self.v[0] = self.v[1]
        self.v[1] = self._coefs[0] * x + self._coefs[1] * self.v[0]

        return self.v[0] + self.v[1]

# Low pass butterworth filter order=2
class FilterBuLp2:

    def __init__(self, coefficients):
        self.v = [0] * 3
        self._coefs = coefficients

    def step(self, x):

        self.v[0] = self.v[1]
        self.v[1] = self.v[2]
        self.v[2] = self._coefs[0] * x + self._coefs[1] * self.v[0] + self._coefs[2] * self.v[1]

        return self.v[0] + self.v[2] + 2 * self.v[1]

class FilterBuLp4:
    def __init__(self, coefficients):
        self.v = [0] * 5

        self._coefs = coefficients

    def step(self, x):

        # Remove old data
        self.v.pop(0)
        self.v.append(
            (self._coefs[0] * x)
            + (self._coefs[1] * self.v[0])
            + (self._coefs[2] * self.v[1])
            + (self._coefs[3] * self.v[2])
            + (self._coefs[4] * self.v[3])
        )

        return self.v[0] + self.v[4] + 4 * (self.v[1] + self.v[3]) + 6 * self.v[2]


# 2nd order butterworth for select alpha
butterworth_lp_2nd_order = {
    0.05: [2.008336556421122521e-2, -0.64135153805756306422, 1.56101807580071816339],
    0.02: [3.621681514928615665e-3, -0.83718165125602272969, 1.82269492519630826877],
    0.2:  [2.065720838261479730e-1, -0.19581571265583305741, 0.36952737735124130403],
}

# 1st order butterworth for select alpha
butterworth_lp_1st_order = {
    0.05: [1.367287359973195227e-1, 0.72654252800536101020],
    0.02: [5.919070381840546569e-2, 0.88161859236318906863],
    0.2:  [4.208077798377318768e-1, 0.15838444032453627419],
    0.4:  [7.547627247472143974e-1, -0.50952544949442879485],
    0.5:  [1.000000000000000000e+0, -1.00000000000000000000]
}

butterworth_lp_4nd_order = {
    0.02: [1.329372889873753349e-5, -0.71991032729187132144, 3.11596692520174567420, -5.06799838673418978630, 3.67172908916193563300],
    0.05: [4.165992044065890132e-4, -0.43826514226197982316, 2.11215535511096863530, -3.86119434899421332119, 3.18063854887471908484],
    0.2:  [4.658290663644368279e-2, -0.03011887504316922495, 0.18267569775303221791, -0.67997852691629923072, 0.78209519802333749006]
}
