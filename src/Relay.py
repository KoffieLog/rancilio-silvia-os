import RPi.GPIO as GPIO
import time

class Relay(object):
    def __init__(self, control_pin=None):
        if not isinstance(control_pin, int):
            raise Exception(f"Control pin must be an integer")
        
        # Save control pin
        self._control_pin = control_pin
        
        # Assert gpio mode
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._control_pin, GPIO.OUT)

        # Start in disabled state
        GPIO.output(self._control_pin, 0)
        self.is_enabled = False
        
    def enable(self):
            GPIO.output(self._control_pin, 1)
            self.is_enabled = True

        
    def disable(self):
            GPIO.output(self._control_pin, 0)
            self.is_enabled = False


# Testing
if __name__ == '__main__':
    r = Relay(control_pin=13)
    
    
    while True:
        if r.is_enabled:
            r.disable()
            print("Relay Off")
        else:
            r.enable()
            print("Relay On")
            
        time.sleep(2)
