# Source Files

* To run the app, simply call on [Main.py](Main.py) from a python terminal. This instantiates the manager objects and 
  performs basic monitoring of app health. For an embedded solution, it's best to launch this script as a
  [startup process](https://www.raspberrypi.org/documentation/linux/usage/systemd.md) on the raspberry pi.
  
* [RancilioManager.py](RancilioManager.py) is responsible for handling sensor polling, PID management, and relay control
  (ie, brewing). These processes take place as background threads, not proper
  [multiprocessing](https://stackoverflow.com/a/3046201) processes, but its good enough for the use case.
  
* [ServerManager.py](ServerManager.py) is responsible for network communication between clients and the 
  RancilioManager. It also wraps processes into threads, including the Flask-SocketIO server which would normally be 
  blocking.
  
* [index.html](templates/index.html) (templates/) is the web dashboard served by the server to clients. This is 
  supported by [index.js](static/js/index.js) and [graph.js](static/js/graph.js) (static/js/) which manage functionality 
  in javascript. 
  
* Other *.py files are supporting objects for either the Rancilio or Server manager. The code has been kept modular in 
order to facilitate modification into derivative works.
