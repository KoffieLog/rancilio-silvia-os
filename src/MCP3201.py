import spidev
import threading
import time

class MCP3201(object):
    # 12 bit discretization
    _bins = 4096
    
    # Maximum update frequency of mcp3201
    _update_freq_max = 976000
    
    def __init__(self, spi_bus, ce_pin, vref):
        if spi_bus not in [0, 1]:
            raise ValueError(f'wrong SPI-bus: {spi_bus} setting (use 0 or 1)!')
        elif ce_pin not in [0, 1]:
            raise ValueError(f'wrong CE-setting: {ce_pin} setting (use 0 for CE0 or 1 for CE1)!')
        elif not isinstance(vref, (int, float)):
            raise Exception(f"Argument {vref} must be an int or float")

        '''
        Initialize the reference voltages'''
        self._vref = vref
        self._dv = vref / (MCP3201._bins - 1)

        """
        Initializes the device, takes SPI bus address (which is always 0 on newer Raspberry models)
        and sets the channel to either CE0 = 0 (GPIO pin BCM 8) or CE1 = 1 (GPIO pin BCM 7) """
        self._spi = spidev.SpiDev()
        self._spi.open(spi_bus, ce_pin)
        self._spi.max_speed_hz = MCP3201._update_freq_max
    
    def read_voltage(self, mode="MSB"):
        val = self._get_adc_output(mode)
        return val * self._dv
    
    def read_bin(self, mode="MSB"):
        return self._get_adc_output(mode)
        
    def _get_adc_output(self, mode):
        if mode == "LSB":
            return self._read_adc_lsb()
        elif mode == "MSB":
            return self._read_adc_msb()
        else:
            raise ValueError(f'Invalid mode: Use "LSB" or "MSB"!')
        
    def _read_adc_msb(self):
        """
        Reads 2 bytes (byte_0 and byte_1) and converts the output code from the MSB-mode:
        byte_0 holds two ?? bits, the null bit, and the 5 MSB bits (B11-B07),
        byte_1 holds the remaining 7 MBS bits (B06-B00) and B01 from the LSB-mode, which has to be removed. """
        bytes_received = self._spi.xfer2([0x00, 0x00])

        msb_1 = bytes_received[1]
        msb_1 = msb_1 >> 1  # shift right 1 bit to remove B01 from the LSB mode

        msb_0 = bytes_received[0] & 0b00011111  # mask the 2 unknown bits and the null bit
        msb_0 = msb_0 << 7  # shift left 7 bits (i.e. the first MSB 5 bits of 12 bits)

        return msb_0 + msb_1

    def _read_adc_lsb(self):
        """
        Reads 4 bytes (byte_0 - byte_3) and converts the output code from LSB format mode:
        byte 1 holds B00 (shared by MSB- and LSB-modes) and B01,
        byte_2 holds the next 8 LSB bits (B03-B09), and
        byte 3, holds the remaining 2 LSB bits (B10-B11). """
        bytes_received = self._spi.xfer2([0x00, 0x00, 0x00, 0x00])

        lsb_0 = bytes_received[1] & 0b00000011  # mask the first 6 bits from the MSB mode
        lsb_0 = bin(lsb_0)[2:].zfill(2)  # converts to binary, cuts the "0b", include leading 0s

        lsb_1 = bytes_received[2]
        lsb_1 = bin(lsb_1)[2:].zfill(8)  # see above, include leading 0s (8 digits!)

        lsb_2 = bytes_received[3]
        lsb_2 = bin(lsb_2)[2:].zfill(8)
        lsb_2 = lsb_2[0:2]  # keep the first two digits

        lsb = lsb_0 + lsb_1 + lsb_2  # concatenate the three parts to the 12-digits string
        lsb = lsb[::-1]  # invert the resulting string
        return int(lsb, base=2)
