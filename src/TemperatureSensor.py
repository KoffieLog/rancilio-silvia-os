import board
import busio
import digitalio
import RPi.GPIO as GPIO
from functools import reduce
from itertools import compress
from threading import Lock


from Sensor import Sensor
from MAX31865 import MAX31865
import time

'''
Handles the polling of sensor data from a temperature sensor

Temp sensor used is MAX31865 RT100.
This has a basic filtering: 3rd order sink 50Hz notch @ -82dB
Maximum polling at ~10HZ '''
class TemperatureSensor(Sensor):
    
    # Error codes for MAX31865
    _fault_table = [
        "RTD High Threshold",
        "RTD Low Threshold",
        "Ref Voltage High",
        "Ref Voltage Low",
        "RTD In Low",
        "Over/Under Voltage"]
    
    def __init__(self):
        
        # Initialize the base sensor class
        super().__init__(
            accumulator_size=10)

        '''
        Temp sensor uses MAX31865 RT100 amplifier from adafruit
        Chip select is on GPIO pin 22 '''
        self._sensor = MAX31865(
            spi=busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO),
            cs=digitalio.DigitalInOut(board.D22),
            ref_resistor=430,
            rtd_nominal=100,
            wires=4,
            filter_frequency=50)

    def _read_sensor(self):
        return self._sensor.temperature
        
    def get_faults(self):
        # RTD high/low errors are not applicable for the MAX31865 driver used
        return list(compress(TemperatureSensor._fault_table[2:], self._sensor.fault[2:]))


if __name__ == '__main__':
    sensor = TemperatureSensor()
    
    for i in range(11):
        
        # Update accumulator
        t, _ = sensor.update_accumulator()
        f = sensor.get_faults()
        
        print(f"Reading {i}")
        print(f"\tTemp: {t}")
        print(f"\tFaults: {f}")
        time.sleep(1/10)

    t = sensor.get_latest_reading(filtered=True)
    print(f"\nFiltered temp: {t}")
    
    input("\nPress Enter to continue...")
    while True:
        sensor.update_accumulator()
        print(sensor.get_latest_reading(filtered=True))
        time.sleep(1/10)
