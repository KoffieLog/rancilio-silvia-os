
// Only call function when document is ready
$(document).ready(function () {


    // Create the socketio object
    var socket = io('http://192.168.0.2:8000');

    // Cache often updated elements
    temp = document.getElementById("temp_value")
    pres = document.getElementById("pres_value")
    boiler_pwm = document.getElementById("boiler_pwm")
    pid_p = document.getElementById("pid_p")
    pid_i = document.getElementById("pid_i")
    pid_d = document.getElementById("pid_d")

    // Color filters for heating svg
    var green_filter = "invert(55%) sepia(69%) saturate(475%) hue-rotate(82deg) brightness(82%) contrast(93%)"
    var black_filter = "invert(0%) sepia(0%) saturate(4304%) hue-rotate(189deg) brightness(107%) contrast(106%)"

    // Socketio: Receive connect event
    socket.on('connect', function () {
        console.log("Connected to server");
    });

    // Receive disconnection event
    socket.on('disconnect', function () {
        console.log("Disconneted from server");
    });

    //------------------------------//
    // ---- INITIAL STATE DATA ---- //
    //------------------------------//

    // Receive initial state event
    socket.on('initial_state', function (data) {

        // Update internal data given initial data
        update_target_temp(data.target);
        set_pid_gains(data.pid_values);
        set_pid_svg_filter(data.pid_auto_mode);
        set_pwm_card(data.pwm);

        // Update plot with buffer data
        update_from_buffer(data.buffer);
    });


    function update_from_buffer(buffer) {

        const n0 = buffer.length;
        const n1 = n0 - 1;
        for(let i = 0; i < n0; i++) {

            const pt = buffer[i];

            if(pt == null)
                continue;

            if(i < n1)
                update_plot(pt, redraw=false);
            else
                update_plot(pt, redraw=true);
        }
    }


    //------------------------------//
    // ---- TARGET TEMPERATURE ---- //
    //------------------------------//

    // Send new target temp from input form to server
    document.getElementById("target_temp").onchange = function () {
        console.log("Target temp update requested at " + this.value);
        socket.emit("update_temp", parseFloat(this.value));
    }

    // Receive new target temperature from the server
    socket.on('brew_temp_updated', function (value) {
        update_target_temp(value)
    });

    // Update the target temp card value
    function update_target_temp(value) {
        updatePlotLine(value)
        target_temp.value = value
        console.log("Target temp updated to: " + value)
    }

    //------------------------------//
    // -------- BREW BUTTON ------- //
    //------------------------------//

    // Send start brew instruction to server
    document.getElementById("brew_svg").onclick = function() {
            console.log("Brew button clicked")
            socket.emit("toggle_brewing")
    }

    // Receive brew started event from server
    socket.on("brewing_started", function () {
        console.log("Brewing started")
        set_brew_button(true)
    });

    // Receive brew complete event from server
    socket.on("brewing_complete", function () {
        console.log("Brewing complete")
        set_brew_button(false)
    });

    // Receive brew interrupted event from server
    socket.on("brewing_interrupted", function () {
        console.log("Brewing interrupted")
        set_brew_button(false)
    });

    // Update the brew button
    function set_brew_button(is_brewing) {

        document.getElementById("brew_svg").style.filter = is_brewing ? green_filter : black_filter
    };

    //------------------------------//
    // --------- PID GAIN --------- //
    //------------------------------//

    // Send new Kp to server
    document.getElementById("kp").onchange = function () {
        console.log("Kp update request sent to server: " + this.value)
        socket.emit("update_pid_gain", [parseFloat(this.value), null, null])
    };

    // Send new Ki to server
    document.getElementById("ki").onchange = function () {
        console.log("Ki update request sent to server: " + this.value)
        socket.emit("update_pid_gain", [null, parseFloat(this.value), null])
    };

    // Send new Kp to server
    document.getElementById("kd").onchange = function () {
        console.log("Kd update request sent to server: " + this.value)
        socket.emit("update_pid_gain", [null, null, parseFloat(this.value)])
    };

    // Receive updated pid values from server
    socket.on("update_pid_gain", function (values) {
        set_pid_gains(values)
    });

    // Update the pid values shown in the card
    function set_pid_gains(values) {
        document.getElementById("kp").value = values[0]
        document.getElementById("ki").value = values[1]
        document.getElementById("kd").value = values[2]
        console.log("PID gains are updated to: " + values)
    };

    //------------------------------//
    // -------- PWM VALUES -------- //
    //------------------------------//

    // Send new PWM values to server
    document.getElementById("boiler_pwm").onchange = function () {
        console.log("PWM update request sent to server: " + this.value)
        socket.emit("update_pwn", parseFloat(this.value));
    };

    // Receive new PWM value from server
    socket.on("update_pwm_value", function (value) {
        set_pwm_card(value)
    })

    // Update the pwm value displayed in the card
    function set_pwm_card(value) {
        boiler_pwm.value = format(value, 1)
    }

    // Send pid toggle notification to server
    document.getElementById("pid_svg").onclick = function () {
        console.log("PID auto mode button clicked")
        socket.emit("toggle_pid_auto")
    }

    // Receive pid auto state from server
    socket.on("pid_auto_updated", function (data) {
        set_pid_svg_filter(data)
    });

    function set_pid_svg_filter(is_auto) {

        // Update color of pid auto button
        document.getElementById("pid_svg").style.filter = is_auto ? green_filter : black_filter;
        result = is_auto ? "Auto" : "Manual";
        console.log("PID mode set to: " + result)
    }

    //------------------------------//
    // ------ STREAMING DATA ------ //
    //------------------------------//

    // Receive streaming data from server
    socket.on("message", function (data) {

        // Update temp plot
        update_plot(data);

        // Update temp and pressure card values
        temp.textContent = format(data.temperature, 2)
        pres.textContent = format(data.pressure, 2);

        // Update pid output card when not in focus
        if (document.activeElement != boiler_pwm) {
            set_pwm_card(data.pwm_value)
        }

        // Update the pid output
        pid_p.textContent = format(data.pid_output[0], 1)
        pid_i.textContent = format(data.pid_output[1], 1)
        pid_d.textContent = format(data.pid_output[2], 1)
    });

    //------------------------------//
    // ------ PID AUTO TUNE ------- //
    //------------------------------//

    // Send autotune event to server
//        document.getElementById("pid_tune_button").onclick = function() {
//        step = $("#pid_auto_step").value
//        socket.emit('pid_autotune', step)
//        console.log("Requested autotune with step " + step)
//    };


    //------------------------------//
    // -------- BOILER DATA ------- //
    //------------------------------//

    socket.on("boiler_update_event", function (state) {

        // Update color of heating card
        document.getElementById("heating_svg").style.filter = state.heating ? green_filter : black_filter;
    });

    //------------------------------//
    // ----------- MISC ----------- //
    //------------------------------//

    // Blur all buttons on click
    $('[type=button]').on("click", function () {
        $(this).blur()
    });


    $('[type=number]').on('click', function () {
        $(this).select()
    })



});



