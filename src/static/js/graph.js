// Global variables
let chart;


// Format floats as 2 decimal place string
const format = (num, decimals) => num.toLocaleString('en-US', {
    minimumFractionDigits: decimals,
    maximumFractionDigits: decimals,
});

// Update the chart time series data
function update_plot(data, redraw=true) {

    // Add point to plot
    const time = data.time;
    const temp_point = [time, data.temperature];
    const pwm_point = [time, data.pwm_value];

    const p_point = [time, data.pid_output[0]]
    const i_point = [time, data.pid_output[1]]
    const d_point = [time, data.pid_output[2]]

    chart.series[0].addPoint(temp_point, false, false);
    chart.series[1].addPoint(pwm_point, false, false);
    chart.series[2].addPoint(p_point, false, false);
    chart.series[3].addPoint(i_point, false, false);
    chart.series[4].addPoint(d_point, false, false);

    if (redraw)
        chart.redraw();
};

// Update the chart target line
function updatePlotLine(value) {

    // Update the target line
    chart.yAxis[0].options.plotLines[0].value = value;
}


$(document).ready(function () {

    // Create the chart object
    chart = new Highcharts.stockChart({

        chart: {
            renderTo: 'main_plot',
            animation: {
                duration: 400,
                easing: function (t) { return t; },
            },
            reflow: true,
//            marginTop: 0,
            spacingTop: 0,
        },

        plotOptions: {

            // Options for all series
            series: {
                // Always use boost
                boostThreshold: 1,
                states: {
                    hover: {
                        enabled: false,
                    },
                }
            }
        },

        boost: {
            enabled: false,
            usePreallocated: false,
            useGPUTranslations: false,
        },

        exporting: {
            enabled: true
        },

        credits: {
            enabled: false
        },

        scrollbar: {
            enabled: false
        },

        rangeSelector: {
            buttons: [{
                count: 1,
                type: 'minute',
                text: '1M'
            }, {
                count: 5,
                type: 'minute',
                text: '5M'
            }, {
                type: 'all',
                text: 'All'
            }],

            inputEnabled: false,
            selected: 2,
            floating: false,
//            y: 25,
        },

        tooltip: {
            animation: false,
            crosshairs: true,
            followPointer: false,
            valueDecimals: 2,
            hideDelay: 100,
            xDateFormat: '%H:%M:%S.%L',
            shared: true,
            split: false,
            hideDelay: 100,
        },

        legend: {
            enabled: true,
            align: 'right',
            verticalAlign: 'top',
            layout: 'horizontal',
            floating: false,
            backgroundColor: '#FFFFFF',
            opacity: 0.5,
//            y: -37,
//            x: -25,
            margin: 0,
            padding: 0,
        },

        xAxis: {
            type: 'datetime',
        },


        yAxis: [
            // Temp Axis
            {
                softMax: 125,
                softMin: 80,
                gridLineWidth: 0,
                opposite: false,
                alignTicks: false,
                plotLines: [{
                    color: 'gray',
                    width: 2,
                    value: 105,
                    dashStyle: 'shortdash',
                    zIndex: 1
                }],
            },
            // PID Axis
            {
                gridLineWidth: 0,
                alignTicks: false,
                opposite: true,

            }
        ],

        series: [{
            name: 'Temperature',
            type: 'line',
            data: [],
            yAxis: 0,
            tooltip: {
                valueSuffix: 'C',
            },
            zIndex: 3,
            color: "#76a9db"
        },
        {
            name: 'PID',
            type: 'line',
            data: [],
            yAxis: 1,
            step: 'left',
            tooltip: {
                valueSuffix: '%',
            },
            zIndex: 2,
            visible: false,
            color: '#000000',
            lineWidth: 1,
        },
        {
            name: 'P Term',
            type: "line",
            data: [],
            yAxis: 1,
            zIndex: 2,
            lineWidth: 1,
            opacity: 1,
            visible: false,
            color: "#78c94d"
        },
        {
            name: 'I Term',
            type: 'line',
            data: [],
            yAxis: 1,
            zIndex: 2,
            lineWidth: 1,
            opacity: 1,
            visible: false,
            color: "#84817a"
        },
        {
            name: 'D Term',
            type: 'line',
            data: [],
            yAxis: 1,
            zIndex: 2,
            lineWidth: 1,
            opacity: 1,
            visible: false,
            color: "#fa983a"
        }],
    });


});

