import eventlet 
eventlet.monkey_patch()
from RancilioManager import RancilioManager
from Configuration import Config
from threading import Thread
from flask import Flask, render_template
from flask_socketio import SocketIO, emit
import time

'''
ServerManager is responsible for sending and receiving messages from clients over WIFI socket connection.
A reference to the instantiated RancilioManager class is required for device control and callbacks
'''
class ServerManager(object):
    def __init__(self, device_manager: RancilioManager):
        # Cache device manager
        self.device = device_manager
        self.nConnections = 0
        self.dataBuffer = [None] * int(600 / Config.server_update_interval)  # 10 min buffer

        # Create the flask framework
        self.app = Flask(__name__)
        self.app.add_url_rule("/", "index", lambda: render_template('index.html'))

        # Create socketio object
        self.socketio = SocketIO(self.app, async_mode="eventlet", logger=False, engineio_logger=False)

        # Register socketio events
        self.socketio.on_event("connect", self.connection)
        self.socketio.on_event('disconnect', self.disconnection)
        self.socketio.on_event("update_temp", self.update_temp)
        self.socketio.on_event("toggle_brewing", self.toggle_brewing)
        self.socketio.on_event("update_pid_gain", self.update_pid_gain)
        self.socketio.on_event("update_pwn", self.update_pid_output)
        self.socketio.on_event("toggle_pid_auto", self.toggle_pid)
        self.socketio.on_event("pid_autotune", self.start_pid_autotune)

        # Register a callback for when the boiler toggles on/off
        self.device.register_callback('boiler', self.push_boiler_state)

        # Frequency of sensor data pushed to clients
        self.server_kwargs = {
            'host': '0.0.0.0',
            'port': 8000,
            'debug': False,
            'use_reloader': False
        }

    # Start the pid autotune process
    def start_pid_autotune(self, value):
        # Set PWM step for autotune
        if value is not None:
            self.device.pid_tuner._outputstep = int(value)
            print("Updated step response to " + value)

        # Run autotune
        self.device.run_auto_tune()

    # Start background data push and socketio process
    def start_async(self):
        # Create background process to buffer data and push buffer to clients
        server_process = Thread(target=self.socketio.run, args=(self.app,), kwargs=self.server_kwargs, daemon=True, name="Server Process")
        push_data_process = Thread(target=self.push_data_loop, args=(Config.server_update_interval,), daemon=True, name="Server Notification Process")

        push_data_process.start()
        server_process.start()

        return server_process, push_data_process

    # Send real-time data to all connected clients
    def push_data_loop(self, interval):
        while True:

            # Latest data
            data = self.device.get_sensor_data()

            # Update data buffer
            self.dataBuffer.append(data)
            self.dataBuffer.pop(0)

            # Broadcast if connected clients
            if self.nConnections:
                self.socketio.send(data)

            # Wait
            time.sleep(interval)

    # Receive connection event from client and emit initialization data back to client
    def connection(self):
        data = self.device.read_device_state()
        data["buffer"] = self.dataBuffer

        emit("initial_state", data, broadcast=False)
        self.nConnections += 1
        print(f"Client connected: {self.nConnections} clients connected.")

    # Receive disconnect event notification from client
    def disconnection(self):
        self.nConnections -= 1
        print(f"Client disconnected: {self.nConnections} clients connected.")

    # Emit current heating state of boiler to all clients
    def push_boiler_state(self, data):
        if self.nConnections:
            self.socketio.emit("boiler_update_event", data)

    # Receive new target temp from client and emit to all
    def update_temp(self, value):
        # Set the new temp and get result
        value = self.device.set_target_temp(value)

        # Emit new temp to all
        emit('brew_temp_updated', value, broadcast=True)
        print(f"Brew temperature updated to {value}")

        # Update settings
        Config.temp_brew = value
        Config.save_settings()

    # Receive brewing toggle instructions, and emit current brew status
    def toggle_brewing(self):
        if self.device.is_brewing():
            self.device.stop_brewing_async()
            emit("brewing_interrupted", broadcast=True)
        else:
            self.device.start_brewing_async(callback=self.brewing_callback)
            emit("brewing_started", broadcast=True)

    # Callback method triggered when brewing complete
    def brewing_callback(self):
        print("Complete brewing")
        self.socketio.emit("brewing_complete", broadcast=True)

    # Receive new pid values from client and emit new values to all
    def update_pid_gain(self, pid_values):

        # Update device pid values and get updated values
        pid_values = self.device.set_pid_gain(*pid_values)

        # Emit new pid values to everyone
        emit("update_pid_gain", pid_values, broadcast=True)
        print(f"PID values set to {pid_values}")

        # Update settings
        Config.kp, Config.ki, Config.kd = pid_values
        Config.save_settings()

    # Receive pid auto toggle instructions and emit new state to all
    def toggle_pid(self):
        auto_mode = self.device.toggle_pid_auto()
        emit("pid_auto_updated", auto_mode, broadcast=True)

    # Receive new heating pwm value and emit new value to all
    def update_pid_output(self, value):
        value = self.device.set_boiler_pwm(value)
        emit("update_pwm_value", value, broadcast=True)
