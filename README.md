# Rancilio Silvia OS

<div align="center">
  <img src="resources/Rancilio_2.PNG" width="100%"/>
</div>

<h3>Motivation</h3>
As lockdown days turned into weeks, turned into months, mornings grew colder. First came the darkness, then came the 
frost... we knew what had to be done; if not for the sake of mental health, certainly for the sake of the diy. A few 
days on and the Rancilio Silvia V6 arrived and all was good... For about a week.

<h3>Bang-Bang control</h3>
It's well known that espresso should be pulled at precise temperatures, in fact my current roast suggests 91-94C. The
trouble is that the temperature control on the machine is binary and controlled by a thermostat which interrupts current
to the boiler when it heats up to ~100C, resulting in large fluctuation in temperature.
The plot below illustrates this, recorded from my machine.
<div align="center">
  <img src="resources/Rancilio_Temp_Base.svg" width="80%"/>
</div>

<h3>Raspberry Pi</h3>
For simplicity, a Raspberry Pi3a+ was chosen as the computer of choice. This is overkill for such a project; in fact
the CPU averages about 15% load. A 
[proto-hat](https://www.adafruit.com/product/2310) was built up with multiple
[relay driver circuits](https://github.com/freetronics/RelayModule4/blob/master/RelayModule4.pdf), an 
[ADC](resources/Datasheets/21290F.pdf) for the
[pressure transducer](resources/Datasheets/2897784.pdf), an
[onboard relay](resources/Datasheets/2787665.pdf) for pump control, and a PT-100
[temp sensor board](https://www.adafruit.com/product/3328) which was cheaper than building my own. 
This is permanently mounted inside the Silvia and operates in a headless configuration, where SSH connection allows for 
easy monitoring and deployment of code. The Pi monitors temperature and pressure, performs relay control, and hosts the 
web interface 24/7. Currently, this has worked for 2 months without any need of intervention. 
From the outside there is zero indication of modification, and all device switches work as intended. 
Code was implemented in a modular OOP manner, allowing strait forward adaptation for derivative projects. 
Illustrated below, testing using a Pi3b+.
<div align="center">
  <img src="resources/Raspberry_pi_3b+.jpg" width="80%"/>
</div>
Don't mind the poor layout, this is a prototype for a later-to-be-designed PCB board and more professional installation.
A box was designed to contain the Pi and proto-hat while allowing cable plugs to connect to their headers. This was
placed securely in the upper compartment of the Silvia.
<div align="center">
  <img src="resources/Pi_case.PNG" width="60%"/>
</div>

<h3>Temperature Sensor</h3>
A thin-film [PT-100](resources/Datasheets/ENG_DS_PTF-FAMILY_A3.pdf) class-A temperature sensor was chosen 
for the project due to its cost, size, accuracy, and response time. The leads of the sensor were soldered to 24AWG 
sensor cable in a 4 wire arrangement. In retrospect smaller AWG would be ideal as soldering 4 cables to 2 leads pitched 
at under a mm was an exercise in patience. The ceramic base was coupled to a ring-terminal with thermal compound, and 
the complete assembly secured with thermally conductive dialectic [epoxy](resources/Datasheets/tds-8329tff-2parts.pdf). 
The ring terminal was screwed to the empty hole of the removed brew thermostat. The temperature sensor provides input to
the PID controller, which modulates an [SSR](resources/Datasheets/FT_SO942460.pdf) supplying power to the boiler heating 
element. Illustrated below, not pretty, but much cheaper than pre-assembled units.
<div align="center">
  <img src="resources/temp_sensor_1.PNG" width="33%"/>
  <img src="resources/temp_sensor_2.PNG" width="33%"/>
  <img src="resources/temp_sensor_3.PNG" width="33%"/>
</div>
Temperature measurements were taken to calibrate the PT-100 sensor reading at the upper boiler surface to the
temperature measured inside the brew basket. A K-type thermocouple was snaked through the portafilter's left outflow 
tract into the brew chamber. A great many factors may effect this temperature: how long the machine is warming,
the temperature of water in the tank, the time between shots, to name a few. For my typical workflow a target
temperature of 104C seems to work well, resulting in ~95.5C in the basket.
<p>&nbsp;</p>
<div align="center">
  <img src="resources/Temp_Calibration_2.PNG" height="300"/>
  <img src="resources/Temp_Calibration.svg" height="300"/>
</div>

<h3>PID Tuning</h3>
Temperature data and was collected at 10Hz following a step response (setting the boiler to 100% duty cycle for some 
seconds). Temperature and duty cycle were mean normalized and used to fit a second-order process with lag-time in 
MATLAB's System Identification toolbox. The fit model (on the right) indeed shows a strong correlation with the 
collected data. This plant model was then used for PID tuning simulations (on the left).
<div align="center">
  <img src="resources/Rancilio_Plant.PNG" width="80%"/>
</div>

A basic PID controller was written in Python, and tied into the core device controller. Typical temperature recovery 
time after pulling a shot is under a minute, with a typical steady state error of below 0.2C. Illustrated below is 
the Silvia warm-up from room temperature to 104C.
<div align="center">
  <img src="resources/Rancilio_Temp_PID.svg" width="80%"/>
</div>

<h3>Interface</h3>
Outside SSHing into the Pi, all interaction with Silvia takes place through the web portal.
A Flask server was implemented to allow any device on the local network access to the device. Illustrated below is 
the web portal from as seen from a desktop PC. Note: The pressure transducer is disconnected in the illustration.
<div align="center">
  <img src="resources/Web_UI_3.PNG" width="80%"/>
</div>

Card elements were adopted from the bootstrap API for device UI. This allows simple expansion of functionality through 
the addition of more cards, while setting the callback functions appropriately. In addition, as the cards follow a 
responsive grid pattern, the same UI can be applied over multiple screen sizes, aspect ratios, and resolutions; 
for example [tablets](resources/Web_UI_4.PNG).
The server streams the latest sensor data to all connected devices at 2Hz, except for the boiler on/off state which is,
in real-time. Any input or requests made by a client is verified for effect by the server, and all clients notified of 
the change in state, ensuring closed loop communication.

Plotting is performed using the HighCharts HighStock API, with animations timed to the real sensor data interval
ensuring a smooth flowing plot. Full-screen togglbility of the chart is implemented, and is visually 
rather nice on mobile devices. The charting library allows multiple data series, where each series can be 
toggled individually, as-well-as windowed zoom mode (smaller blue time-series below the main plot).

<h3>Power</h3>
This Rancilio model complies with EU norms requiring a means to power off after some time of inactivity. 
There is a microcomputer manufactured by Gicar located next to the pump, under the water tank responsible for this. 
Originally the Pi was to be powered from the 5V output of the onboard voltage regulator, however after checking
the [spec sheet](resources/Datasheets/78L05A_datasheet.pdf) this was abandoned due to current limits. A
discrete 5V [power supply](resources/Datasheets/2787665.pdf) was opted for and feeds into the 5V rail of the 
Pi. The illustration below highlights the onboard microcomputer board for those interested. The 4 pin header is used
for programming the [microcontroller](resources/Datasheets/stm8s003f3_datasheet.pdf), but can also supply limited 5V DC.
<div align="center">
  <img src="./resources/Rancilio_Microcomputer.png" width="80%"/>
</div>

<h3>Future Work</h3>
Removal of the Gicar microcontroller. This requires minor rework of the wiring harness. Power is already controllable
from the Pi via a bidirectional MOSFET [circuit](resources/Bidirectional_mosfet.PNG) which shorts the 12V power sense 
wires on the Gicar microcontroller. 
Pressure control via PWM.

<h3>Notes</h3>
After collecting the metal shavings from drilling through the chassis to mount one of the relays, oxidative attempts
suggest that the chassis material for the V6 Silvia is indeed stainless steel, an improvement from earlier
generations.








